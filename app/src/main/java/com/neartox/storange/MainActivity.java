package com.neartox.storange;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity
implements View.OnClickListener {
    private final static String TAG = "MyInternalStorage";
    private Button mButtonLoad;
    private Button mButtonSave;
    private TextInputLayout mSourceFilenameLayout;
    private TextInputEditText mSourceFilenameEdit;
    private TextInputLayout mSourceContentLayout;
    private TextInputEditText mSourceContentEdit;


    private TextInputEditText mSource1FilenameEdit;
    private TextInputEditText mSource1ContentEdit;

    private static Toast mToast;

    private void SendToast(String text) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButtonLoad = (Button) findViewById(R.id.button_load);
        mButtonSave = (Button) findViewById(R.id.button_save);
        mSourceFilenameLayout = (TextInputLayout) findViewById(R.id.source_filename_layout);
        mSourceFilenameEdit = (TextInputEditText) findViewById(R.id.source_filename_text);
        mSourceContentLayout = (TextInputLayout) findViewById(R.id.source_content_layout);
        mSourceContentEdit = (TextInputEditText) findViewById(R.id.source_content_text);

        mSource1FilenameEdit = (TextInputEditText) findViewById(R.id.source1_filename_text);
        mSource1ContentEdit = (TextInputEditText) findViewById(R.id.source1_content_text);
        mButtonLoad.setOnClickListener(this);
        mButtonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_load) {
            loadFromStorage();
        } else if (view.getId() == R.id.button_save) {
            boolean hasError = false;
            // Validate filename
            if (mSourceFilenameEdit.getText().toString().isEmpty()) {
                hasError = true;
                mSourceFilenameLayout.setError("File must not be empty");
            } else {
                mSourceFilenameLayout.setError(null);
            }
            // Validate content
            if (mSourceContentEdit.getText().toString().isEmpty()) {
                hasError = true;
                mSourceContentLayout.setError("Content must not be empty");
            } else {
                mSourceContentLayout.setError(null);
            }
            if (!hasError) {
                saveStorage();
            }

        }
    }

    private void loadFromStorage() {
        try {
            String filename = mSource1FilenameEdit.getEditableText().toString().trim();
            FileInputStream fis = openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            isr.close();
            fis.close();
            mSource1ContentEdit.setText(sb.toString());
        } catch (IOException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
            SendToast(e.getLocalizedMessage());
        }
    }

    private void saveStorage() {
        FileOutputStream fos = null;
        try {
            String filename = mSourceFilenameEdit.getEditableText().toString().trim();
            String content = mSourceContentEdit.getEditableText().toString().trim();
            fos = openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(content.getBytes());
            fos.flush();
            fos.close();
            SendToast("Content Saved to '" + filename + "' successfully");
        } catch (IOException e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
            SendToast(e.getLocalizedMessage());
        }
    }
}